# Daily ui libraries digest

## 21 June *(1396/03/31)*

1. [Android-InfiniteCards](https://github.com/BakerJQ/Android-InfiniteCards)
1. [BoxLoaderView](https://github.com/nipun-birla/BoxLoaderView)
1. [FeaturedRecyclerView](https://github.com/developer-shivam/FeaturedRecyclerView)
1. [RippleValidatorEditText](https://github.com/omidheshmatinia/RippleValidatorEditText)
1. [SectorProgressView](https://github.com/timqi/SectorProgressView)
1. [DownloadProgress](https://github.com/aloj22/DownloadProgress)

## 22-28 June *(1396/04/01-1396/04/07)*

1. [android-api-graph_flow](https://github.com/saantiaguilera/android-api-graph_flow)
1. [VectorMaster](https://github.com/harjot-oberai/VectorMaster)
1. [DraggableTreeView](https://github.com/jakebonk/DraggableTreeView)
1. [glazy-viewpager](https://github.com/kannan-anbu/glazy-viewpager)
1. [duo-navigation-drawer](https://github.com/PSD-Company/duo-navigation-drawer)
1. [cardslider-android](https://github.com/Ramotion/cardslider-android)
1. [show-case-card-view](https://github.com/dimorinny/show-case-card-view)
1. [OdometerLibrary](https://github.com/Chils17/OdometerLibrary)