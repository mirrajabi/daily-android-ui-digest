# Daily ui libraries digest

## 1 July *(1396/04/10)*

1. [FabulousFilter](https://github.com/Krupen/FabulousFilter)
1. [TooltipIndicator](https://github.com/huseyinozer/TooltipIndicator)
1. [graph_flow](https://github.com/saantiaguilera/android-api-graph_flow)

## 2 July *(1396/04/11)*

1. [LiquidRadioButton](https://github.com/omidheshmatinia/LiquidRadioButton)
1. [material-searchview](https://github.com/Shahroz16/material-searchview)

## 3-4 July *(1396/04/12-13)*

1. [Spotlight](https://github.com/TakuSemba/Spotlight)
1. [SeekBarWithIntervals](https://github.com/RameshBhupathi/SeekBarWithIntervals)
1. [RangeBarVertical](https://github.com/ITheBK/RangeBarVertical)

## 5 July *(1396/04/14)*

1. [terminal-seekbar](https://github.com/alshell7/terminal-seekbar)
1. [steet_view_markers](https://github.com/alkurop/steet_view_markers)
1. [aurora-imui](https://github.com/jpush/aurora-imui)
1. [EasyBehavior](https://github.com/JmStefanAndroid/EasyBehavior)
1. [Muzli Weekly Inspiration for Designers #110](https://medium.muz.li/weekly-inspiration-for-designers-110-2ffd381552b1)

## 6-8 July *(1396/04/15-17)*

1. [SmartRefreshLayout](https://github.com/scwang90/SmartRefreshLayout)
1. [PopsTabView](https://github.com/ccj659/PopsTabView)
1. [RubberStamp](https://github.com/vinaygaba/RubberStamp)

## 9-12 July *(1396/04/18-21)*

1. [SegmentedProgressBar](https://github.com/carlosmuvi/SegmentedProgressBar)
1. [RichPath](https://github.com/tarek360/RichPath)
1. [datepicker-timeline](https://github.com/badoualy/datepicker-timeline)
1. [AwesomeKeyboard](https://github.com/hoanganhtuan95ptit/AwesomeKeyboard)

## 13-18 July *(1396/04/22-27)*

1. [android-dial-picker](https://github.com/moldedbits/android-dial-picker)
1. [ImagePicker-OLX](https://github.com/RameshBhupathi/ImagePicker-OLX)
1. [DayNightSwitch](https://github.com/Mahfa/DayNightSwitch)
1. [FaceDetector](https://github.com/Fotoapparat/FaceDetector)
1. [SeparateShapesView](https://github.com/steelkiwi/SeparateShapesView)
1. [YuuPlayer](https://github.com/agusibrahim/YuuPlayer)
1. [SimpleDialog](https://github.com/BROUDING/SimpleDialog)
1. [ProminentColor](https://github.com/mathiazhagan01/ProminentColor)
1. [sc-player](https://github.com/Paroca72/sc-player)
1. [Android-ProgressViews](https://github.com/zekapp/Android-ProgressViews)
1. [ChatBar](https://github.com/CenkGun/ChatBar)

## 19-23 July *(1396/04/28-1396/05/01)*

1. [FileListerDialog](https://github.com/FirzenYogesh/FileListerDialog)
1. [Parallax](https://github.com/imablanco/Parallax)
1. [EditCodeView](https://github.com/Onum/EditCodeView)
1. [StoriesProgressView](https://github.com/shts/StoriesProgressView)